#include "Retangulo.h"

@implementation Retangulo
- (void) setAltura: (int) novaAltura
{
  altura = novaAltura;
}

- (void) setLargura: (int) novaLargura
{
  largura = novaLargura;
}

- (void) setAltura: (int) novaAltura eLargura: (int) novaLargura
{
  [self setAltura: novaAltura];
  [self setLargura: novaLargura];
}

- (int) largura
{
  return largura;
}

- (int) altura
{
  return altura;
}

- (int) area
{
  return largura * altura;
}

- (int) perimetro
{
  return (largura + altura) * 2;
}
@end
