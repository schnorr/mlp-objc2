#ifndef __RETANGULO_H_
#define __RETANGULO_H_
#include <Foundation/Foundation.h>

@interface Retangulo : NSObject
{
  int largura;
  int altura;
}
- (void) setAltura: (int) novaAltura;
- (void) setLargura: (int) novaLargura;
- (void) setAltura: (int) novaAltura eLargura: (int) novaLargura;
- (int) largura;
- (int) altura;
- (int) area;
- (int) perimetro;
@end

#endif
