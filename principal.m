#include <Foundation/Foundation.h>
#include "Retangulo.h"
#include "Quadrado.h"

int main (int argc, char **argv)
{
  {
    Retangulo *retangulo = [[Retangulo alloc] init];

    [retangulo setAltura: 5 eLargura: 8];

    NSLog (@"Retangulo: altura = %d, largura = %d", [retangulo altura], [retangulo largura]);
    NSLog (@"Retangulo: area = %d, perimetro = %d", [retangulo area], [retangulo perimetro]);

    [retangulo release];
  }

  {
    Quadrado *quadrado = [[Quadrado alloc] init];

    [quadrado setLado: 5];

    NSLog (@"Quadrado: lado = %d", [quadrado lado]);
    NSLog (@"Quadrado:  area = %d, perimetro = %d", [quadrado area], [quadrado perimetro]);

    [quadrado release];

  }
  return 0;
}
