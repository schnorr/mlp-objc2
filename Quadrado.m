#include "Quadrado.h"

@implementation Quadrado
- (void) setLado: (int) novoLado
{
  [self setAltura: novoLado eLargura: novoLado];
}

- (int) lado
{
  return [self altura];
}
@end
