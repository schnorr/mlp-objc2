#include <Foundation/Foundation.h>

//Fracao
@interface Fracao : NSObject
- (void) adicionar: (Fracao*) outraFracao;
- (void) print;
@end

@implementation Fracao
- (void) adicionar: (Fracao*) outraFracao { }
- (void) print { }
@end

//Complexo
@interface Complexo : NSObject
- (void) adicionar: (Complexo*) outroComplexo;
- (void) print;
@end

@implementation Complexo
- (void) adicionar: (Complexo*) outroComplexo { }
- (void) print { }
@end

int main (int argc, char **argv)
{
  Fracao *f1 = [[Fracao alloc] init];
  Fracao *f2 = [[Fracao alloc] init];

  Complexo *c1 = [[Complexo alloc] init];
  Complexo *c2 = [[Complexo alloc] init];

  [f1 adicionar: f2];
  [c1 adicionar: c2];

  [f1 print];
  [c1 print];

  [f1 release];
  [f2 release];
  [c1 release];
  [c2 release];

  return 0;
}
