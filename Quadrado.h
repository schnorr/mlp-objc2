#ifndef __QUADRADO_H_
#define __QUADRADO_H_
#include <Foundation/Foundation.h>
#include "Retangulo.h"

@interface Quadrado : Retangulo
{
}
- (void) setLado: (int) novoLado;
- (int) lado;
@end

#endif
