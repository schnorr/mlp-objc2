#include <Foundation/Foundation.h>

//Declaração de ClassA (subclasse de NSObject)
@interface ClassA : NSObject
{
  int x;
}
- (void) initVar;
@end

//Implementação de ClassA
@implementation ClassA
- (void) initVar
{
  x = 100;
}
@end


//Declaração de ClassB (subclasse de ClassA)
@interface ClassB : ClassA
- (void) printVar;
@end

//Implementação de ClassB
@implementation ClassB
- (void) printVar
{
  NSLog (@"x = %d", x);
}
@end

int main (int argc, char **argv)
{
  ClassB *b = [[ClassB alloc] init];

  [b initVar]; // utiliza o método herdado
  [b printVar]; // revela o valor de x

  NSLog (@"%@", [b description]); // imprime a descrição de b

  [b release];
  return 0;
}
